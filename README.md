Проект создан ради [Wiki](https://gitlab.com/kmm-ga/readme/wikis/readme)<br>
[Как писать практичные сообщения к коммитам](https://anvilabs.co/blog/writing-practical-commit-messages/)<br>

# Автозагрузка в windows 10.<br>
Создаём файл c:\autorun.cmd <br>
с содержимым <br>
```cmd
@start cmd /k C:\директория_со_стратегиями\стратегия\файл_запуска_стратегии.cmd
@ping localhost -n 7 > nul
@pause
```
Ярлык на файл c:\autorun.cmd ложим в директорию
```
C:\Users\%username%\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup
```

Создаём файл AutoLogon.reg <br>
меняем "username" и "password" на свои.<br>
```
Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon]
"DefaultUserName"="username"
"DefaultPassword"="password"
"AutoAdminLogon"="1"
```
запускаем, соглашаемся. <br>

>  ---
>  ---

# Клонирование репозитория в локальную директорию.<br>

[Клонирование репозитория в локальную директорию.](https://gitlab.com/kmm-ga/readme/wikis/%D0%9A%D0%BB%D0%BE%D0%BD%D0%B8%D1%80%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5-%D1%80%D0%B5%D0%BF%D0%BE%D0%B7%D0%B8%D1%82%D0%BE%D1%80%D0%B8%D1%8F-%D0%B2-%D0%BB%D0%BE%D0%BA%D0%B0%D0%BB%D1%8C%D0%BD%D1%83%D1%8E-%D0%B4%D0%B8%D1%80%D0%B5%D0%BA%D1%82%D0%BE%D1%80%D0%B8%D1%8E)<br>

>  ---
>  ---